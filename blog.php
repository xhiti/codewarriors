<?php require_once("Includes/DB.php"); ?>
<?php require_once("Includes/Functions.php"); ?>
<?php require_once("Includes/Sessions.php"); ?>

<?php echo confirmLogin(); ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv
="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<link rel="stylesheet" href="css/styling.css">
<link href="images/first.jpg" rel="shortcut icon"/>
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
<link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/footer.css">
<link rel="stylesheet" href="css/index.css">
<link href="images/first.jpg" rel="shortcut icon"/>

<title>Blog Page</title>
<style media="screen">
.heading{
    font-family: Bitter,Georgia,"Times New Roman",Times,serif;
    font-weight: bold;
     color: #005E90;
}
.heading:hover{
  color: #0090DB;
}

.statistika{
  color:black;
  font-size:20px;
}

.same:hover{
  background-color:red;
}
</style>
</head>
<body>

<!-- HEADER START -->
<nav class="sm-navbar navbar navbar-expand-lg">
      <div class="container2">
          <div class="sm-logo">
              <a href="blog.php?page=1" style="margin-left:-140px"><img src="images/cw.png" width="110px" height="40px"></a>
          </div>

          <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <?php
            if (confirmLogin() == true){

              $ID = $_SESSION["userid"];
              $sql      = "select * from admins where id = $ID";
              $result   = mysqli_query($connectingDB, $sql);
              $row      = mysqli_fetch_array($result);
              $role     = $row['role'];

              if ($role === 'user'){
            ?>
            <ul class="sm-nav-menu" style="margin-left:-90px;">
                <li><a href="myposts.php" class="nav-links">Posts</a></li>
                <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i>&nbsp; My Profile</a></li>
                <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                <li><a href="statistics.php" class="nav-links2">Statistics</a></li>
                <li><a href="logout.php" class="nav-links3"><i class="fas fa-user-times" style="color:red;"></i> Logout</a></li>
            </ul>

            <ul style="float:right;" class="navbar-nav ml-auto">
                <form class="form-inline d-none d-sm-block" action="blog.php" style="margin-right:-150px;">
                    <div class="form-group">
                        <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                        <button  class="btn btn-primary" name="SearchButton">Go</button>
                    </div>
                </form>
            </ul>
          <?php } elseif ($role == 'admin'){ ?>
            <ul class="sm-nav-menu" style="margin-left:-70px;">
                <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                <li><a href="posts.php" class="nav-links2">Posts</a></li>
                <li><a href="categories.php" class="nav-links">Categories</a></li>
                <li><a href="manageUsers.php" class="nav-links2">Users</a></li>
                <li><a href="comments.php" class="nav-links2">Comments</a></li>
                <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i> &nbsp;My Profile</a></li>
                <li><a href="logout.php" class="nav-links3"><i class="fas fa-user-times" style="color:red;"></i> Logout</a></li>
            </ul>

            <ul style="float:right;" class="navbar-nav ml-auto">
                <form class="form-inline d-none d-sm-block" action="blog.php" style="margin-right:-120px;">
                    <div class="form-group">
                        <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                        <button  class="btn btn-primary" name="SearchButton">Go</button>
                    </div>
                </form>
            </ul>

          <?php } ?>
        <?php } ?>

          </div>
</nav>
<!-- HEADER END -->



  <!-- HEADER -->

  <div class="container">
    <div class="row mt-4">

      <!-- Main Area Start-->

      <div class="col-sm-8 "> <br><br><br>
        <h2 style="color: #0C0613;  "> <span style="font-weight: bold">    <?php
              echo ErrorMessage();
              ?><br>   </span></h2>

        <h2 style="color: #0C0613;  ">  CodeWarriors <span style="font-weight: bold">  Blog <br>   </span></h2>

        <br><br>
        <!-- <h1 style="color: #0C0613; font-weight: bold">CODE WARRIORS Blog</h1> <br><br> -->
        <?php
        global $connectingDB;
        // SQL query when Searh button is active

        if(isset($_GET["SearchButton"])){
          $Search = $_GET["Search"];
          $sql = "SELECT * FROM posts
          WHERE datetime LIKE '%{$Search}%'
          OR title LIKE '%{$Search}%'
          OR post LIKE '%{$Search}%'";
          $result = mysqli_query($connectingDB, $sql);
        }// Query When Pagination is Active i.e Blog.php?page=1
        elseif (isset($_GET["page"])) {
          $Page = $_GET["page"];
          if($Page==0||$Page<1){
          $ShowPostFrom=0;
        }else{
          $ShowPostFrom=($Page*5)-5;
        }
          $sql ="SELECT * FROM posts ORDER BY id desc LIMIT $ShowPostFrom,5";
          $result = mysqli_query($connectingDB, $sql);
        }
        // Query When Category is active in URL Tab
        elseif (isset($_GET["category"])) {
          $Category = $_GET["category"];
          // $sql = "SELECT * FROM posts WHERE category_id=$Category ORDER BY id desc";
          $sql = "SELECT * FROM posts p JOIN category c ON p.category_id = c.id
                  WHERE category_id=$Category ";
          $result = mysqli_query($connectingDB, $sql);
        }

        // The default SQL query
        else{
          $sql  = "SELECT * FROM posts ORDER BY id desc";
          $result = mysqli_query($connectingDB, $sql);
        }

        while ($DataRows = mysqli_fetch_array($result)) {
          $PostId          = $DataRows[0];
          $DateTime        = $DataRows["dateTime"];
          $PostTitle       = $DataRows[2];
          $CategoryId        = $DataRows["category_id"];
          $Image           = $DataRows["image"];
          $PostDescription = $DataRows["post"];
          $user_id         = $DataRows["user_id"];

        ?>
        <div class="card"> <br>
          <h4 class="card-title"><?php echo htmlentities($PostTitle); ?></h4> <br>
          <img src="upload/<?php echo htmlentities($Image); ?>" style="max-height:450px;" class="img-fluid card-img-top" />
          <div class="card-body">
           <small class="text-muted">Category: <span class="text-dark">
               <a style="color:darkblue;" href="blog.php?category=<?php echo $CategoryId; ?>"> <?php
                                                                                             global $connectingDB;
                                                                                             $sql = "SELECT * FROM category WHERE id=$CategoryId ";
                                                                                             $res = mysqli_query($connectingDB, $sql);
                                                                                             $row = mysqli_fetch_array($res);
                                                                                             echo htmlentities($row["title"]); ?> </a> &nbsp;
              </span> & Written by <span style="color:darkblue;" class="text-dark"> <a style="color:darkblue;"><b>
                <?php
                  global $connectingDB;
                  $sql = "SELECT * FROM admins WHERE id=$user_id";
                  $res = mysqli_query($connectingDB, $sql);
                  $Row = mysqli_fetch_array($res);
                  echo htmlentities($Row["aname"]); ?>
              </b></a></span> &nbsp; On <span style="color:darkblue;" class="text-dark"><b><?php echo htmlentities($DateTime); ?></b></span></small>
              <span style="float: right;" class="badge" >
                <i class="fa fa-thumbs-up statistika"></i> <span class="statistika" id="nrlikes"> <?php echo getLikes($PostId); ?> </span> &nbsp;
                <i class="fa fa-thumbs-down statistika"></i> <span class="statistika" id="nrdislikes"> <?php echo getDislikes($PostId); ?> </span> &nbsp;
                <i class="fas fa-comment-alt statistika"></i> <span class="statistika" id="nrcomments"> <?php echo ApproveCommentsAccordingtoPost($PostId); ?> </span>
            </span>
            <hr>
            <p class="card-text">
              <?php if (strlen($PostDescription)>150) { $PostDescription = substr($PostDescription,0,150)."...";} echo htmlentities($PostDescription); ?></p>
            <a href="FullPost.php?id=<?php echo $PostId; ?>" style="float:right;">
              <span class="btn btn-info">Read More &rang;&rang; </span>
            </a>
          </div>
        </div>
        <br>
        <?php   } ?>
        <!-- Pagination -->
        <nav>
          <ul class="pagination pagination-lg">
            <!-- Creating Backward Button -->
            <?php if( isset($Page) ) {
              if ( $Page>1 ) {?>
           <li class="page-item">
               <a href="Blog.php?page=<?php  echo $Page-1; ?>" class="page-link">&laquo;</a>
             </li>
           <?php } }?>
          <?php
          global $connectingDB;
          $sql           = "SELECT COUNT(*) FROM posts";
          $result         = mysqli_query($connectingDB, $sql);
          $RowPagination = mysqli_fetch_array($result);
          $TotalPosts    = array_shift($RowPagination);
          // echo $TotalPosts."<br>";
          $PostPagination=$TotalPosts/5;
          $PostPagination=ceil($PostPagination);
          // echo $PostPagination;
          for ($i=1; $i <=$PostPagination ; $i++) {
            if( isset($Page) ){
              if ($i == $Page) {  ?>
            <li class="page-item active">
              <a href="blog.php?page=<?php  echo $i; ?>" class="page-link"><?php  echo $i; ?></a>
            </li>
            <?php
          }else {
            ?>  <li class="page-item">
                <a href="Blog.php?page=<?php  echo $i; ?>" class="page-link"><?php  echo $i; ?></a>
              </li>
          <?php  }
        } } ?>
        <!-- Creating Forward Button -->
        <?php if ( isset($Page) && !empty($Page) ) {
          if ($Page+1 <= $PostPagination) {?>
       <li class="page-item">
           <a href="blog.php?page=<?php  echo $Page+1; ?>" class="page-link">&raquo;</a>
         </li>
       <?php } }?>
          </ul>
        </nav>
      </div>
      <!-- Main Area End-->

      <!-- Side Area Start -->
      <div class="col-sm-4">
        <div class="card mt-4">
          <div class="card-body">
            <img src="images/startblog.png" class="d-block img-fluid mb-3" alt="">
            <div class="text-center">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
          </div>
        </div>
        <br>
        <div class="card">
          <div class="card-header text-light">
            <h2 class="lead" >CODE WARRIORS !</h2>
          </div>
          <div class="card-body">
            <div class="card-header bg-info text-white same">
              <a href="login.php" class="lead" name="button"> Log In / Register</a>
            </div> <br>
            <div class="card-header bg-info text-white same">
              <a href="statistics.php" class="lead" name="button">Statistics about our Blog</a>
            </div> <br>

          </div>
        </div>
        <br>

        <!-- START CATEGORIES -->
        <div class="card">
          <div class="card-header bg-info text-white">
            <h2 class="lead"> CATEGORIES</h2>
          </div>
            <div class="card-body">
              <?php
              global $connectingDB;
              $sql = "SELECT * FROM category ORDER BY id desc";
              $result = mysqli_query($connectingDB, $sql);
              while ($DataRows = mysqli_fetch_array($result)){
                $CategoryId = $DataRows["id"];
                $CategoryName=$DataRows["title"];
               ?>
              <a href="blog.php?category=<?php echo $CategoryId; ?>"> <span style="font-size:18px;" class="heading"> <?php echo $CategoryName; ?></span> </a><br>

             <?php } ?>
          </div>
        </div>
        <!-- END CATEGORIES -->

        <br>

        <!-- RECENT POSTS START -->
        <div class="card">
          <div class="card-header bg-info text-white">
            <h2 class="lead"> Recent Posts</h2>
          </div>
          <div class="card-body">
            <?php
            global $connectingDB;
            $sql= "SELECT * FROM posts ORDER BY id desc LIMIT 0,5";
            $result = mysqli_query($connectingDB, $sql);
            while ($DataRows = mysqli_fetch_array($result)) {
              $Id     = $DataRows['id'];
              $Title  = $DataRows['title'];
              $DateTime = $DataRows['dateTime'];
              $Image = $DataRows['image'];
            ?>
            <div class="media">
              <img src="upload/<?php echo htmlentities($Image); ?>" class="d-block img-fluid align-self-start"  width="90" height="94" alt="">
              <div class="media-body ml-2">
              <a style="text-decoration:none;" href="FullPost.php?id=<?php echo htmlentities($Id) ; ?>" target="_blank">  <h6 class="lead"><?php echo htmlentities($Title); ?></h6> </a>
                <p class="small"><?php echo htmlentities($DateTime); ?></p>
              </div>
            </div>
            <hr>
            <?php } ?>
          </div>
        </div>
        <!-- RECENT POSTS END -->

      </div>
      <!-- Side Area End -->


    </div>

  </div>

  <!-- HEADER END -->
<br>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>


<script>
$('#year').text(new Date().getFullYear());
</script>


<?php require("includes/footer.php"); ?>


</body>
</html>
