<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php $_SESSION['TrackingURL'] = $_SERVER["PHP_SELF"]; ?>


<?php if( checkAdmin()){
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comments</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <script src="js/bootstrap.js"></script>
</head>
<body>

  <!-- HEADER START -->
  <nav class="sm-navbar navbar navbar-expand-lg">
        <div class="container2">
            <div class="sm-logo">
                <a href="blog.php?page=1" style="margin-left:-140px"><img src="images/cw.png" width="110px" height="40px"></a>
            </div>

            <div class="collapse navbar-collapse" id="navbarcollapseCMS">
                <ul class="sm-nav-menu" style="margin-left:-70px;">
                  <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                  <li><a href="posts.php" class="nav-links2">Posts</a></li>
                  <li><a href="categories.php" class="nav-links">Categories</a></li>
                  <li><a href="manageUsers.php" class="nav-links2">Users</a></li>
                  <li><a href="comments.php" class="nav-links2">Comments</a></li>
                  <li><a href="statistics.php" class="nav-links2">Statistics</a></li>
                  <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i> &nbsp;My Profile</a></li>
                  <li><a href="logout.php" class="nav-links3"><i class="fas fa-user-times" style="color:red;"></i> Logout</a></li>
              </ul>

            </div>
          </div>
  </nav>
  <!-- HEADER END -->

  <header class="bg-dark text-white py-3">
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <h1 style="margin-top:-40px;"><i class='fas fa-comments' style='font-size:40px; color:#27aae1;'></i> Manage Comments</h1>
              </div>
          </div>
      </div>
  </header>

<br>

<!--MAIN AREA-->
<section class="container py-2 mb-4">
    <div class="row">
        <div class="col-md-12">
            <h2>Un-Approved Comments</h2>
            <table class="table table-striped table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Date&Time</th>
                        <th>Comment</th>
                        <th>Approve</th>
                        <th>Action</th>
                        <th>Details</th>
                    </tr>
                </thead>


            <?php
                $sql = "select * from comments where status = 'OFF' order by id desc";
                $result = mysqli_query($connectingDB, $sql);

                $no_comments = 0;
                while ($row = mysqli_fetch_array($result)){
                    $comment_id = $row['id'];
                    $dateTime = $row['dateTime'];
                    $comment_content = $row['comment'];
                    $comment_post_id = $row['post_id'];
                    $no_comments++;

                      $UserId = $row["user_id"];
                      $query = "SELECT * FROM admins WHERE id=$UserId ";
                      $res = mysqli_query($connectingDB, $query);
                      $data = mysqli_fetch_array($res);
                      $username = $data["aname"];

                    if (strlen($username) > 10) { $username = substr($username, 0, 10)."..."; }
                    if (strlen($dateTime) > 10) { $dateTime = substr($dateTime, 0, 10)."..."; }
                    if (strlen($comment_content) > 80) { $comment_content = substr($comment_name, 0, 80)."..."; }
                ?>
                <tbody>
                    <tr>
                        <td><?php echo htmlentities($no_comments); ?></td>
                        <td><?php echo htmlentities($dateTime) ?></td>
                        <td><?php echo htmlentities($username); ?></td>
                        <td><?php echo htmlentities($comment_content); ?></td>
                        <td><a href="approveComments.php?id=<?php echo $comment_id; ?>" class="btn btn-success">Approve</a></td>
                        <td><a href="deleteComments.php?id=<?php echo $comment_id; ?>" class="btn btn-danger">Delete</a></td>
                        <td><a class="btn btn-primary" href="fullPost.php?id=<?php echo $comment_post_id; ?>" target="_blank">Live Preview</a></td>
                    </tr>
                </tbody>
                <?php } ?>
            </table>
            <h2>Approved Comments</h2>
            <table class="table table-striped table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>Date&Time</th>
                    <th>Comment</th>
                    <th>Dis-Approve</th>
                    <th>Action</th>
                    <th>Details</th>
                </tr>
                </thead>


                <?php
                $sql = "select * from comments where status = 'ON' order by id desc";
                $result = mysqli_query($connectingDB, $sql);

                $no_comments = 0;
                while ($row = mysqli_fetch_array($result)){
                    $comment_id = $row['id'];
                    $dateTime = $row['dateTime'];
                    $comment_content = $row['comment'];
                    $comment_post_id = $row['post_id'];
                    $no_comments++;

                    $UserId = $row["user_id"];
                    $query = "SELECT * FROM admins WHERE id=$UserId ";
                    $res = mysqli_query($connectingDB, $query);
                    $data = mysqli_fetch_array($res);
                    $username = $data["aname"];

                    if (strlen($username) > 10) { $username = substr($username, 0, 10)."..."; }
                    if (strlen($dateTime) > 10) { $dateTime = substr($dateTime, 0, 10)."..."; }
                    if (strlen($comment_content) > 80) { $comment_content = substr($comment_name, 0, 80)."..."; }
                    ?>
                    <tbody>
                    <tr>
                        <td><?php echo htmlentities($no_comments); ?></td>
                        <td><?php echo htmlentities($dateTime) ?></td>
                        <td><?php echo htmlentities($username); ?></td>
                        <td><?php echo htmlentities($comment_content); ?></td>
                        <td><a href="disApproveComments.php?id=<?php echo $comment_id; ?>" class="btn btn-warning">Dis-Approve</a></td>
                        <td><a href="deleteComments.php?id=<?php echo $comment_id; ?>" class="btn btn-danger">Delete</a></td>
                        <td><a class="btn btn-primary" href="fullPost.php?id=<?php echo $comment_post_id; ?>" target="_blank">Live Preview</a></td>
                    </tr>
                    </tbody>
                <?php } ?>
            </table>
        </div>
    </div>
</section>
<!--END MAIN AREA-->

<?php require("includes/footer.php"); ?>

</body>
</html>
<?php } else {
    $_SESSION["ErrorMessage"] = "You are not allowed to do this operation";
    header("location: blog.php?page=1");
} ?>
