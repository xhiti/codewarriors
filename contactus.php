<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php
    if (isset($_POST['contact_us'])){
        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];

        if (empty($name) || empty($email) || empty($message)){
            $_SESSION["ErrorMessage"] = "All fields must be filled out!";
        }
        elseif (!empty($email)) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = filter_var($email, FILTER_VALIDATE_EMAIL);
                redirect_to("https://script.google.com/macros/s/AKfycbz80rbLKV7pFEw_qgPwyZu4hKNQdyRGgZE5_MYvQjudhDjQl9_V/exec");
                redirect_to("thank_you.html");
            } else {
                $_SESSION["ErrorMessage"] = "Invalid email format!";
            }
        }
    }
?>
<?php
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM admins WHERE username='$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);
    $role = $row["role"];

    if($role == "user"){
        ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CodeWarriors | Contact Us</title>
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <!--<link rel="stylesheet" href="css/styles.css">-->
    <link rel="stylesheet" href="css/contactus.css">
    <link rel="stylesheet" href="css/footer.css">
</head>
<body>
<!--NAVBAR-->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu">
                <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                <?php
                    if(checklogin() === true){ ?>
                        <li><a href="login.php" class="nav-links3">Logout</a></li>
                <?php } else {?>
                        <li><a href="login.php" class="nav-links2">Login</a></li>
                <?php } ?>
            </ul>
            <ul style="float:right;" class="navbar-nav ml-auto">
                <form class="form-inline d-none d-sm-block" action="blog.php">
                    <div class="form-group">
                        <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                        <button  class="btn btn-primary" name="SearchButton">Go</button>
                    </div>
                </form>
            </ul>
        </div>
    </div>
</nav>
<div style="height: 70px; background: #27aae1"></div>
<div class="container-contact100">
    <div class="wrap-contact100">
        <form class="contact100-form validate-form" action="https://script.google.com/macros/s/AKfycbz80rbLKV7pFEw_qgPwyZu4hKNQdyRGgZE5_MYvQjudhDjQl9_V/exec" method="post" data-email="from_email@example.com">
				<span class="contact100-form-title">
					Send Us A Message
				</span>
            <?php echo errorMessage(); ?>
            <div class="wrap-input100 validate-input" data-validate="Name is required">
                <label class="label-input100" for="name">Full name</label>
                <input id="name" class="input100" type="text" name="name" placeholder="Enter your name...">
                <span class="focus-input100"></span>
            </div>
            <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                <label class="label-input100" for="email">Email Address</label>
                <input id="email" class="input100" type="text" name="email" placeholder="Enter your email...">
                <span class="focus-input100"></span>
            </div>
            <div class="wrap-input100 validate-input" data-validate = "Message is required">
                <label class="label-input100" for="message">Message</label>
                <textarea id="message" class="input100" name="message" placeholder="Type your message here..."></textarea>
                <span class="focus-input100"></span>
            </div>
            <div class="col-md-12">
                <!--contact button-->
                <button class="btn-big btn btn-bg contact100-form-btn" id="contact_us" name="contact_us">
                    Send Us <i class="fas fa-arrow-right"></i>
                </button>
            </div>
        </form>
        <div class="contact100-more flex-col-c-m" style="background-image: url('images/contact_us_bg.jpg');">
            <div class="sp-container">
                <div class="sp-content">
                    <div class="sp-globe"></div>
                    <h2 class="frame-1">Have any question?</h2>
                    <h2 class="frame-2">Contact Us</h2>
                    <h2 class="frame-3">Fill form to the right</h2>
                    <h2 class="frame-4">We will contact you soon!</h2>
                    <h2 class="frame-5">
                        <span>Fork,</span>
                        <span>Change,</span>
                        <span>Experience.</span>
                    </h2>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once ('includes/footer.php'); ?>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
<script data-cfasync="false" type="text/javascript" src="js/form-submission-handler.js"></script>
</html>
<?php }
else{ header("location: dashboard.php");?>
    <div class="container">
        <?php $_SESSION["ErrorMessage"] = "You are not allowed to do this operation"; ?>
    </div>
<?php  }?>
