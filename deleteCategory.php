<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>

<?php
if (isset($_GET['id'])){
    $search_query_parameter = $_GET['id'];
    $admin = $_SESSION['username'];
    //$sql = "delete from category where id = '$search_query_parameter'";
    $sql = "SELECT * FROM posts p JOIN category c ON p.category_id = c.id
                     WHERE category_id=$search_query_parameter";
    $result = mysqli_query($connectingDB, $sql);

    if (mysqli_num_rows($result) > 0){
        $sql = "delete from category where id = '$search_query_parameter'";
        $result = mysqli_query($connectingDB, $sql);
        $sql = "delete posts from posts p where p.category_id = '$search_query_parameter'";
        $result = mysqli_query($connectingDB, $sql);
        header("Location: categories.php");
        $_SESSION["ErrorMessage"] = "Category cannot be deleted!";
    }
    elseif (mysqli_num_rows($result) == 0){
        $sql = "delete from category where id = '$search_query_parameter'";
        $result = mysqli_query($connectingDB, $sql);
        header("Location: categories.php");
        $_SESSION["SuccessMessage"] = "Category deleted successfully!";
    }
    else{
        header("Location: categories.php");
        $_SESSION["ErrorMessage"] = "Something went wrong! Try again!";
    }
}
?>