<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>

<?php
    if(checkUser()){
?>

<?php

$SearchQueryParameter = $_GET['id'];

if(isset($_POST["Submit"])){
    $PostTitle = $_POST["PostTitle"];
    $CategoryTitle  = $_POST["Category"];
    $Image     = $_FILES["Image"]["name"];
    $Target    = "upload/".basename($_FILES["Image"]["name"]);
    $PostText  = $_POST["PostDescription"];
    $Admin     = "CodeWarriors";
    date_default_timezone_set("Europe/Tirane");
    $CurrentTime = time();
    $DateTime    = strftime("%B-%d-%Y %H:%M:%S",$CurrentTime);

    if(empty($PostTitle)){
        $_SESSION["ErrorMessage"]= "Title Cant be empty";
        Redirect_to("Posts.php");
    }elseif (strlen($PostTitle)<5) {
        $_SESSION["ErrorMessage"]= "Post Title should be greater than 5 characters";
        Redirect_to("Posts.php");
    }elseif (strlen($PostText)>9999) {
        $_SESSION["ErrorMessage"]= "Post Description should be less than than 1000 characters";
        Redirect_to("Posts.php");
    }else{

        $query = "SELECT * FROM category WHERE title='$CategoryTitle'";
        $res = mysqli_query($connectingDB, $query);
        $row = mysqli_fetch_array($res);
        $categoryID = $row['id'];

        // Query to Update Post in DB When everything is fine
        if (!empty($_FILES["Image"]["name"])) {    // sigurohemi qe nese modifikojme vetem nje nga fushat e formes imazhi mos te fshihet
            $sql = "UPDATE posts
                  SET title='$PostTitle', image='$Image', post='$PostText', dateTime='$DateTime', category_id=$categoryID
                  WHERE id='$SearchQueryParameter'";
            move_uploaded_file($_FILES["Image"]["tmp_name"],$Target);
        }else {
            $sql = "UPDATE posts
                  SET title='$PostTitle', post='$PostText', dateTime='$DateTime', category_id=$categoryID
                  WHERE id='$SearchQueryParameter'";
        }
        $result = mysqli_query($connectingDB, $sql);
        if($result){
            $_SESSION["SuccessMessage"]="Post Updated Successfully";
            //redirect_to("posts.php");
        }else {
            $_SESSION["ErrorMessage"]= "Something went wrong. Try Again !";
            //redirect_to("posts.php");
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Posts</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <script src="js/bootstrap.js"></script>
</head>
<body>
<!--NAVBAR-->
<div style="height: 5px; background: #27aae1"></div>
<!--NAVBAR START -->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu">
              <li><a href="blog.php" class="nav-links">BLOG</a></li>
              <li><a href="addNewPost.php" class="nav-links">CREATE POST</a></li>
              <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i> &nbsp;MY PROFILE</a></li>
              <li><a href="aboutus.php" class="nav-links2">ABOUT US</a></li>
              <li><a href="contactus.php" class="nav-links2">CONTACT US </a></li>
              <li><a href="login.php" class="nav-links2">REGISTER</a></li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li  class="nav-item"><a id="logouti" href="logout.php" class="nav-link text-danger"><i style="color:red" class="fas fa-user-times"></i> <span style="color:white">LOGOUT</span></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- <div style="height: 70px; background: #27aae1"></div> -->
<!-- NAVBAR END -->
<div style="height: 5px; background: #27aae1"></div>
<!--NAVBAR END-->

<!--HEADER-->
<header class="bg-dark text-white py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="margin-top:-50px;"><i class="fas fa-edit" style="color: #27aae1;"></i> Edit Post</h1>
            </div>
        </div>
    </div>
</header>
<!--HEADER END-->
<br>

<!--MAIN AREA-->
<!-- Main Area -->
<section class="container py-2 mb-4">
    <div class="row">
        <div class="offset-lg-1 col-lg-10" style="min-height:400px;">
            <?php
                echo ErrorMessage();
                echo SuccessMessage();

                $SarchQueryParameter = $_GET["id"];

                $sql  = "SELECT * FROM posts WHERE id=$SarchQueryParameter";
                $result = mysqli_query($connectingDB, $sql);
                $DataRows = mysqli_fetch_array($result);
                $TitleToBeUpdated    = $DataRows['title'];
                $CategoryId          = $DataRows['category_id'];
                $ImageToBeUpdated    = $DataRows['image'];
                $PostToBeUpdated     = $DataRows['post'];
            ?>
            <form class="" action="editPost.php?id=<?php echo $SarchQueryParameter; ?>" method="post" enctype="multipart/form-data">
                <div class="card bg-secondary text-light mb-3">
                    <div class="card-body bg-dark">
                        <div class="form-group">
                            <label for="title"> <span class="fieldInfo"> Post Title: </span></label>
                            <input class="form-control" type="text" name="PostTitle" id="title" placeholder="Type title here" value="<?php echo $TitleToBeUpdated; ?>">
                        </div>
                        <div class="form-group">
                            <span class="fieldInfo">Existing Category: </span>
                            <span class="fieldInfo" style="color:#00FFFF"> <?php
                            $sql = "SELECT * FROM category WHERE id=$CategoryId";
                            $res = mysqli_query($connectingDB, $sql);
                            $row = mysqli_fetch_array($res);
                            echo $row["title"];
                            ?> </span>
                            <br>
                            <label for="CategoryTitle"> <span class="fieldInfo"> Chose Categroy </span></label>
                            <select class="form-control" id="CategoryTitle"  name="Category">
                                <?php
                                //Fetchinng all the categories from category table
                                $sql  = "SELECT id,title FROM category";
                                $result = mysqli_query($connectingDB, $sql);
                                while ($DataRows = mysqli_fetch_array($result)) {
                                    $Id            = $DataRows["id"];
                                    $CategoryName  = $DataRows["title"];
                                    ?>
                                    <option> <?php echo $CategoryName; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form=group mb-1">
                            <span class="fieldInfo">Existing Image: </span>
                            <img src="upload/<?php echo $ImageToBeUpdated; ?>" class="mb-1" width="170px;" height="100px"><br><br>
                            <div class="custom-file">
                                <input class="custom-file-input" type="File" name="Image" id="imageSelect" value="">
                                <label for="imageSelect" class="custom-file-label">Select Image </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Post"> <span class="fieldInfo"> Post: </span></label>
                            <textarea class="form-control" id="Post" name="PostDescription" rows="8" cols="80">
                <?php echo $PostToBeUpdated;?>
              </textarea>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 mb-2">
                                <a href="#" class="btn btn-warning btn-block"><i class="fas fa-arrow-left"></i> Back To Your Posts</a>
                            </div>
                            <div class="col-lg-6 mb-2">
                                <button type="submit" name="Submit" class="btn btn-success btn-block">
                                    <i class="fas fa-check"></i> &nbsp;  EDIT
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!--END MAIN AREA-->

<!--FOOTER-->
<?php require_once("includes/footer.php"); ?>

<!--FOOTER END-->

<?php } else {
      $_SESSION["ErrorMessage"] = "You are not allowed to do this operation";
      header("location: dashboard.php");
  }
?>
</body>
</html>
