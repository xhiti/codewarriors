<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>

<?php echo confirmLogin(); ?>

<?php require_once("server.php"); ?>

<?php
if (isset($_POST["Submit"])){
    $user_id = $_SESSION["userid"];
    $comment = $_POST["CommenterThoughts"];
    $post_id = $_GET['id'];
    $status = "OFF";


    date_default_timezone_set("Europe/London");
    $currentTime = time();
    $dateTime = strftime("%B-%d-%Y %H:%M:%S", $currentTime);

    if (empty($comment)){
        $_SESSION["ErrorMessage"] = "All fields must be filled out!";
        //redirect_to("fullPost.php?id={$_GET['id']}");
    }
    elseif (strlen($comments) > 500){
        $_SESSION["ErrorMessage"] = "Comment should be less than 500 characters!";
        //redirect_to("fullPost.php?id={$_GET['id']}");
    }
    else{

      /*
      $user_id = $_SESSION["userid"];
      $sql = "SELECT * FROM admins WHERE id=$user_id";
      $result = mysqli_query($connectingDB, $sql);
      $data = mysqli_fetch_array($result);
      $user = $data['username'];
      */


        $query = "insert into comments(dateTime, comment, post_id, status, user_id) values (?, ?, ?, ?, ?)";
        $result = mysqli_prepare($connectingDB, $query);



        if ($result){
            mysqli_stmt_bind_param($result, "ssisi", $dateTime, $comment, $post_id, $status, $user_id);
            mysqli_stmt_execute($result);
            $last_id = mysqli_insert_id($connectingDB);
            $_SESSION["SuccessMessage"] = "Comment added successfully";
            //redirect_to("fullPost.php?id={$_GET['id']}");
        }
        else{
            $_SESSION["ErrorMessage"] = "Something went wrong! Try again!";
            //redirect_to("fullPost.php?id={$_GET['id']}");
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CodeWarriors | Blog Page</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/my.css">
    <style>
      .not{
        color:grey;
      }
      .fa:hover {
       color: darkblue;
      }

      .statistika{
        color:black;
        font-size:20px;
      }

      #logouti{
        color: darkblue;
      }

      .heading{
          font-family: Bitter,Georgia,"Times New Roman",Times,serif;
          font-weight: bold;
           color: #005E90;
      }
      .heading:hover{
        color: #0090DB;
      }

      .infos:hover{
        color: #0090DB;
      }

    </style>

</head>
<body>

  <!-- HEADER START -->
  <nav class="sm-navbar navbar navbar-expand-lg">
        <div class="container2">
            <div class="sm-logo">
                <a href="blog.php?page=1" style="margin-left:-140px"><img src="images/cw.png" width="110px" height="40px"></a>
            </div>

            <div class="collapse navbar-collapse" id="navbarcollapseCMS">
              <?php
              if (confirmLogin() == true){

                $ID = $_SESSION["userid"];
                $sql      = "select * from admins where id = $ID";
                $result   = mysqli_query($connectingDB, $sql);
                $row      = mysqli_fetch_array($result);
                $role     = $row['role'];

                if ($role === 'user'){
              ?>
              <ul class="sm-nav-menu" style="margin-left:-90px;">
                <li><a href="myposts.php" class="nav-links">Blog</a></li>
                  <li><a href="myposts.php" class="nav-links">Posts</a></li>
                  <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i>&nbsp; My Profile</a></li>
                  <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                  <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                  <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                  <li><a href="statistics.php" class="nav-links2">Statistics</a></li>
                  <li><a href="logout.php" class="nav-links3"><i class="fas fa-user-times" style="color:red;"></i> Logout</a></li>
              </ul>

            <?php } elseif ($role == 'admin'){ ?>
              <ul class="sm-nav-menu" style="margin-left:-70px;">
                  <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                  <li><a href="posts.php" class="nav-links2">Posts</a></li>
                  <li><a href="categories.php" class="nav-links">Categories</a></li>
                  <li><a href="manageUsers.php" class="nav-links2">Users</a></li>
                  <li><a href="comments.php" class="nav-links2">Comments</a></li>
                  <li><a href="myprofile.php" class="nav-links2"><i class="fas fa-user text-success"></i> &nbsp;My Profile</a></li>
                  <li><a href="logout.php" class="nav-links3"><i class="fas fa-user-times" style="color:red;"></i> Logout</a></li>
              </ul>

            <?php } ?>
          <?php } ?>

            </div>
  </nav>
  <!-- HEADER END -->

<!--HEADER-->
<div class="container">
    <div class="row mt-4">
        <div class="col-sm-8">

            <br><br>
            <?php
                echo errorMessage();
                echo successMessage();
            ?>
            <?php

                $post_id_from_url = $_GET['id'];

                if (!isset($post_id_from_url)){
                    $_SESSION['ErrorMessage'] = "Bad Request!";
                    //redirect_to("blog.php");
                }

                $sql = "SELECT * FROM posts WHERE id=$post_id_from_url ";
                $result = mysqli_query($connectingDB, $sql);
                if ($result){
                while ($row = mysqli_fetch_array($result)) {
                    unset($title);
                    $post_id = $row['id'];
                    $title = $row['title'];
                    $categoryId = $row['category_id'];
                    $dateTime = $row['dateTime'];
                    $image = $row['image'];
                    $post = $row['post'];
                    $user_id = $row["user_id"];
                    ?>

                    <div class="card"> <br>
                        <h2 class="card-title"><?php echo htmlentities($title); ?> </h2> <br>
                        <img src="upload/<?php echo htmlentities($image); ?>" class="img-fluid card-img-top" style="height: 500px!important;">
                        <div class="card-body"> &nbsp;&nbsp;&nbsp;&nbsp;
                            <i <?php if (userLiked($post_id)){ ?>
                                  class="fa fa-thumbs-up"
                                <?php } else { ?>
                                  class="fa fa-thumbs-up not"
                                <?php } ?>
                               style="font-size:40px;" postid="<?php echo $post_id; ?>"  id="butoni1" title="I LIKE THIS"></i>



                            &nbsp;&nbsp;&nbsp;&nbsp;

                          <i <?php if (userDisliked($post_id)){ ?>
                              class="fa fa-thumbs-down"
                            <?php } else { ?>
                              class="fa fa-thumbs-down not"
                            <?php } ?>
                            style="font-size:40px;" postid="<?php echo $post_id; ?>" id="butoni2" title="I DONT'T LIKE THIS"></i> <br><br>

                            Category: <b><a style="color:darkblue;"> <?php
                                                               global $connectingDB;
                                                               $sql = "SELECT * FROM category WHERE id=$categoryId ";
                                                               $res = mysqli_query($connectingDB, $sql);
                                                               $r = mysqli_fetch_array($res);
                                                               echo htmlentities($r["title"]); ?> </a></b> <br>
                            Written by <b style="color:darkblue;"><?php
                                            global $connectingDB;
                                            $sql = "SELECT * FROM admins WHERE id=$user_id";
                                            $res = mysqli_query($connectingDB, $sql);
                                            $Row = mysqli_fetch_array($res);
                                            echo htmlentities($Row["aname"]);
                                          ?></b> &nbsp;&nbsp; On <b style="color:darkblue;"><?php echo htmlentities($dateTime); ?></b></small>
                            <span style="float: right;" class="badge" >
                              <i class="fa fa-thumbs-up statistika"></i> <span class="statistika" id="nrlikes"> <?php echo getLikes($post_id); ?> </span> &nbsp;
                              <i class="fa fa-thumbs-down statistika"></i> <span class="statistika" id="nrdislikes"> <?php echo getDislikes($post_id); ?> </span> &nbsp;
                              <i class="fas fa-comment-alt statistika"></i> <span class="statistika" id="nrcomments"> <?php echo ApproveCommentsAccordingtoPost($post_id); ?> </span>
                          </span>
                            <div style="height: 10px; background: white"></div>
                            <hr>
                            <p><?php echo htmlentities($post); ?></p>
                        </div>
                    </div>
                    <div style="height: 10px; background: white"></div>
                <?php } } ?>
                <br>

                <!-- Comment Part Start -->
                <!-- Fetching existing comment START  -->
            <span class="FieldInfo"><b>Comments (<?php echo ApproveCommentsAccordingtoPost($post_id); ?>) </b></span><br><br>
            <?php
                $sql = "select * from comments where post_id = $post_id AND status='ON' ";
                $result = mysqli_query($connectingDB, $sql);

                while ($row = mysqli_fetch_array($result)){
                    $comment_id = $row["id"];
                    $comment_date = $row['dateTime'];
                    $comment_post = $row['comment'];
                    //$username = $row["username"];
                    $userid = $row["user_id"];
            ?>

            <div>
                <div class="media commentBlock">
                    <img src="images/user-512.png" class="d-block img-fluid align-self-start" width="10%;">
                    <div class="media-body ml-2">
                        <h6 class="lead"> <?php
                                          //echo htmlentities($username);
                                          $sql = "SELECT * FROM admins WHERE id=$userid ";
                                          $res = mysqli_query($connectingDB, $sql);
                                          $row = mysqli_fetch_array($res);
                                          echo htmlentities($row["aname"]);
                                        ?></h6>
                        <p class="small"><?php echo $comment_date; ?></p>
                        <p><?php echo $comment_post; ?></p>
                    </div>
                </div>
            </div> <hr>

            <?php } ?>
            <!--  Fetching existing comment END -->

         <div>
           <form class="" action="FullPost.php?id=<?php echo $post_id ?>" method="post">
             <div class="card mb-3">
               <div class="card-header">
                 <h5 class="FieldInfo">Share your thoughts about this post</h5>
               </div>
               <div class="card-body">
                 <div class="form-group">
                   <textarea name="CommenterThoughts" class="form-control" rows="6" cols="80"></textarea>
                 </div>
                 <div class="">
                   <button type="submit" name="Submit" class="btn btn-primary">Submit</button>
                 </div>
               </div>
             </div>
           </form>
         </div>
           <!-- Comment Part End -->
      </div>

        <!-- SIDE AREA START -->
        <div class="col-sm-4" style="padding-top: 45px;">
                <div class="card mt-4">
                    <img src="./images/startblog.PNG" alt="">
                    <div class="card-body">
                        <p>Start a blog now & create your posts!</p>
                        <p>Make a post in our blog to share it with your friends and find out even more or you just want to publish your achievement?</p>
                        <p>Share your ideas and much more!</p>
                        <a href="addNewPost.php" style="padding-left: 75px;">
                            <span class="btn btn-primary">Create/Add Post</span>
                        </a>
                    </div>
                  </div>
                <br>

                <!-- START CATEGORIES -->
                <div class="card">
                  <div class="card-header bg-primary text-light">
                    <h2 class="lead">Categories</h2>
                    </div>
                    <div class="card-body">
                      <?php
                      global $connectingDB;
                      $sql = "SELECT * FROM category ORDER BY id desc";
                      $result = mysqli_query($connectingDB, $sql);
                      while ($DataRows = mysqli_fetch_array($result)){
                        $CategoryId = $DataRows["id"];
                        $CategoryName=$DataRows["title"];
                       ?>
                      <a href="blog.php?category=<?php echo $CategoryId; ?>"> <span class="heading"> <?php echo $CategoryName; ?></span> </a><br>
                     <?php } ?>
                  </div>
                </div>


                <!-- END CATEGORIES -->

                <br><br>
                <br>
                <br>

                <!-- START RECENT POSTS -->
                <div class="card">
                  <div class="card-header bg-info text-white">
                    <h2 class="lead"> Recent Posts</h2>
                  </div>
                  <div class="card-body">
                    <?php
                    global $connectingDB;
                    $sql= "SELECT * FROM posts ORDER BY id desc LIMIT 0,5";
                    $result = mysqli_query($connectingDB, $sql);
                    while ($DataRows = mysqli_fetch_array($result)) {
                      $Id     = $DataRows['id'];
                      $Title  = $DataRows['title'];
                      $DateTime = $DataRows['dateTime'];
                      $Image = $DataRows['image'];
                    ?>
                    <div class="media">
                      <img src="upload/<?php echo htmlentities($Image); ?>" class="d-block img-fluid align-self-start"  width="90" height="94" alt="">
                      <div class="media-body ml-2">
                      <a style="text-decoration:none;"href="FullPost.php?id=<?php echo htmlentities($Id) ; ?>" target="_blank">  <h6 class="lead"><?php echo htmlentities($Title); ?></h6> </a>
                        <p class="small"><?php echo htmlentities($DateTime); ?></p>
                      </div>
                    </div>
                    <hr>
                    <?php } ?>
                  </div>
                </div>
                <!-- END RECENT POSTS -->
    </div>
 <!-- Side Area End -->
</div>
</div>



<br>
<script>

$(document).ready(function(){

// if the user clicks on the like button ...
$('#butoni1').on('click', function(){
        var post_id = $(this).attr("postid");
        var action = "";
        $clicked_btn = $(this);
        if ($clicked_btn.hasClass('fa-thumbs-up not')) {
  	    action = 'like';
       } else if($clicked_btn.hasClass('fa-thumbs-up')){
  	     action = 'unlike';
        }

        $.post('fullPost.php?id=' + post_id, {
        		"action" : action,
        		"post_id" : post_id
        	},
          function(data){

            if (action == "like") {
        			$clicked_btn.removeClass('fa-thumbs-up not');
        			$clicked_btn.addClass('fa-thumbs-up');
        		} else if(action == "unlike") {
        			$clicked_btn.removeClass('fa-thumbs-up');
        			$clicked_btn.addClass('fa-thumbs-up not');
  		       }
        });
   });

   $('#butoni2').on('click', function(){
           var post_id = $(this).attr("postid");
           var action = "";
           $clicked_btn = $(this);
           if ($clicked_btn.hasClass('fa-thumbs-down not')) {
     	    action = 'dislike';
        } else if($clicked_btn.hasClass('fa-thumbs-down')){
     	     action = 'undislike';
           }
           $.post('fullPost.php?id=' + post_id, {
           		"action" : action,
           		"post_id" : post_id
           	},
             function(data){
               // alert(post_id + " " + action);
               if (action == "dislike") {
           			$clicked_btn.removeClass('fa-thumbs-down not');
           			$clicked_btn.addClass('fa-thumbs-down');
           		} else if(action == "undislike") {
           			$clicked_btn.removeClass('fa-thumbs-down');
           			$clicked_btn.addClass('fa-thumbs-down not');
     		       }
           });
      });
});

// class="fa fa-thumbs-down not"
</script>



<?php require_once("includes/footer.php"); ?>
</body>
</html>
