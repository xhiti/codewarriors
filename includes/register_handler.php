<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php
global $connectingDB;
global $password;
global $user_id;
if (isset($_POST["register"])) {
    $username = strip_tags($_POST["username_reg"]);          //remove html tags
    $username = str_replace('','',$username);  //remove spaces
    /*$username = ucfirst(strtolower($username)); */        //uppercase first letter
    $_SESSION["name_reg"] = $username;

    $name = strip_tags($_POST["name_reg"]);
    $name = str_replace('','',$name);
    $name = ucfirst(strtolower($name));
    $_SESSION["name_reg"] = $name;

    $email = strip_tags($_POST["email_reg"]);
    $email = str_replace(" ", "", $email);
    /*$email = ucfirst(strtolower($email));*/
    $_SESSION["email_reg"] = $email;

    $password = strip_tags($_POST["password_reg"]);
    $confirm_password = strip_tags($_POST["confirm_password_reg"]);
    $admin = "";
    $role = "user";
    $aheadline = "";
    $abio = "";


    date_default_timezone_set("Europe/London");
    $currentTime = time();
    $dateTime = strftime("%B-%d-%Y %H:%M:%S", $currentTime);

    if (empty($username) || empty($name) || empty($password) || empty($confirm_password) || empty($email)){
        $_SESSION["ErrorMessage"] = "All fields must be filled out!";
        //redirect_to("categories.php");s
    }
    elseif (strlen($password) < 5){
        $_SESSION["ErrorMessage"] = "Password should be greater than 5 characters!";
        //redirect_to("categories.php");
    }
    elseif ($password !== $confirm_password){
        $_SESSION["ErrorMessage"] = "Password and Confirm Password should match!";
        //redirect_to("categories.php");
    }
    elseif (preg_match('/[^A-Za-z0-9]/', $password)){
        $_SESSION["ErrorMessage"] = "Your password can contain only english characters and numbers!";
    }
    elseif (checkUsernameExistsOrNot($username)){
        $_SESSION["ErrorMessage"] = "Username exists! Try another one!";
    }
    elseif (!empty($email)){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            $email = filter_var($email, FILTER_VALIDATE_EMAIL);
            $sql = "select email from admins where email = '$email'";
            $result = mysqli_query($connectingDB, $sql);
            $email_check = mysqli_query($connectingDB, $sql);

            $num_rows = mysqli_num_rows($email_check);

            if($num_rows > 0){
                $_SESSION["ErrorMessage"] = "Email already in use!";
            }
            elseif(!preg_match("~@fshnstudent\.info$~",$email)){
                $_SESSION["ErrorMessage"] = "Email should be in this form: example@fshnstudent.info!";
            }
            else{
                $password = md5($password);
                $profile_pic = "head_wet_asphalt.png";

                $query = "insert into admins(dateTime, username, password, aname, email, aheadline, abio, aimage, addedby, role) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                $result = mysqli_prepare($connectingDB, $query);

                if ($result){
                    mysqli_stmt_bind_param($result, "ssssssssss", $dateTime, $username, $password, $name, $email, $aheadline, $abio, $profile_pic, $admin, $role);
                    mysqli_stmt_execute($result);
                    $last_id = mysqli_insert_id($connectingDB);
                    $_SESSION["SuccessMessage"] = "Successfully registered! Go ahead and login!";
                    //redirect_to("basic.html");
                }
                else{
                    $_SESSION["ErrorMessage"] = "Something went wrong! Try again!";
                    //redirect_to("categories.php");
                }
                mysqli_stmt_close($result);
                mysqli_close($connectingDB);
            }
        }
        else{
            $_SESSION["ErrorMessage"] = "Invalid email format!";
        }
    }
}
?>