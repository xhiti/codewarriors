<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php
    if (isset($_POST['contact_us'])){
        if (empty($_POST['name']) || empty($_POST['email']) || $_POST['message']){
            $_SESSION["ErrorMessage"] = "All fields must be filled out!";
        }
        elseif (!empty($_POST['email'])){
            if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                $_POST['email'] = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
                redirect_to("thank_you.html");
            }
            else{
                $_SESSION["ErrorMessage"] = "Invalid email format!";
            }
            redirect_to("thank_you.html");
        }
    }
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>CodeWarriors | Homepage</title>
    <meta charset="UTF-8">
    <!-- Favicon -->
    <link href="images/first.jpg" rel="shortcut icon"/>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="css/about.css"/>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="css/index.css"/>
</head>
<body>
<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>
<!-- Header section -->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu" style="float: right; width: 100%">
                <li><a href="index.php" class="nav-links">Home</a></li>
                <li><a href="blog.php?page=1" class="nav-links2">Blog</a></li>
                <li><a href="#posts" class="nav-links2">Posts</a></li>
                <li><a href="#statistics" class="nav-links2">Statistics</a></li>
                <li><a href="#about" class="nav-links2">About Us</a></li>
                <li><a href="#contact" class="nav-links2">Contact Us</a></li>
                <li><a href="login.php" class="nav-links2">Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<div style="height:10px; background:#27aae1;"></div>
<!-- Hero section -->
<section class="hero-section overflow-hidden">
    <div class="hero-slider owl-carousel">
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="images/slider-bg-1.jpg">
            <div class="container">
                <h2>Welcome!</h2>
                <p>“You can work quite hard, in particular online, and do quite well independently,<br> but if you really want to grow you need points of leverage and most of them come from knowing people.”</p>
            </div>
        </div>
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="images/slider-bg-2.jpg">
            <div class="container">
                <h2>CodeWarriors</h2>
                <p>“You can work quite hard, in particular online, and do quite well independently,<br> but if you really want to grow you need points of leverage and most of them come from knowing people.”</p>
            </div>
        </div>
    </div>
</section>
<!-- Blog section -->
<br>
<br>
<br>
<div id="posts" class="container3"><br>
    <div class="sectiontitle">
        <h2>Latest Posts</h2>
        <span class="headerLine"></span><br><br>
    </div><br><br>
    <section id="jarak">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators poindic">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="card-deck">
                            <?php
                            $sql = "select * from posts order by id desc limit 0,3";
                            $result = mysqli_query($connectingDB, $sql);

                            while ($row = mysqli_fetch_array($result)){
                                $post_id = $row['id'];
                                $title = $row['title'];
                                $dateTime = $row['dateTime'];
                                $image = $row['image'];
                                $post = $row['post'];
                                ?>
                                <div class="col">
                                    <div class="card">
                                        <img src="upload/<?php echo $image; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?php if (strlen($title) > 40) { $title = substr($title, 0, 40)."..."; } echo htmlentities($title); ?></h3>
                                            <p><?php if (strlen($post) > 115) { $post = substr($post, 0, 115)."..."; } echo htmlentities($post); ?></p>
                                        </div>
                                        <div class="card-footer">
                                            <small class="text-muted"><?php echo $dateTime; ?></a></small>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="card-deck">
                            <?php
                            $sql = "select * from posts order by case when id=3 then +1 else id end limit 3";
                            $result = mysqli_query($connectingDB, $sql);

                            while ($row = mysqli_fetch_array($result)){
                                $post_id = $row['id'];
                                $title = $row['title'];
                                $dateTime = $row['dateTime'];
                                $image = $row['image'];
                                $post = $row['post'];
                                ?>
                                <div class="col">
                                    <div class="card">
                                        <img src="upload/<?php echo $image; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?php if (strlen($title) > 30) { $title = substr($title, 0, 30)."..."; } echo htmlentities($title); ?></h3>
                                            <p><?php if (strlen($post) > 100) { $post = substr($post, 0, 100)."..."; } echo htmlentities($post); ?></p>
                                        </div>
                                        <div class="card-footer">
                                            <small class="text-muted"><?php echo $dateTime; ?></a></small>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="card-deck">
                            <?php
                            $sql = "select * from posts order by case when id=6 then +1 else id end limit 3";
                            $result = mysqli_query($connectingDB, $sql);

                            while ($row = mysqli_fetch_array($result)){
                                $post_id = $row['id'];
                                $title = $row['title'];
                                $dateTime = $row['dateTime'];
                                $image = $row['image'];
                                $post = $row['post'];
                                ?>
                                <div class="col">
                                    <div class="card">
                                        <img src="upload/<?php echo $image; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?php if (strlen($title) > 30) { $title = substr($title, 0, 30)."..."; } echo htmlentities($title); ?></h3>
                                            <p><?php if (strlen($post) > 100) { $post = substr($post, 0, 100)."..."; } echo htmlentities($post); ?></p>
                                        </div>
                                        <div class="card-footer">
                                            <small class="text-muted"><?php echo $dateTime; ?></small>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><br><br>
</div>
<div style="height: 30px;"></div>
<a href="blog.php?page=1" class="site-btn" id="contact_us" name="go_to_blog" style="text-align: center; margin-left: 44.5%;">Go to Blog  <img src="images/icons/double-arrow.png" alt="#"/></a>
<br>
<br>
<br>
<!-- Statistics section-->
<br>
<div id="statistics_section">
    <div id="statistics" class="sectiontitle">
        <h2>Blog statistics</h2>
        <span class="headerLine"></span>
    </div>
    <div id="projectFacts" class="sectionClass">
        <div class="fullWidth eight columns">
            <div class="projectFactsWrap ">
                <div class="item wow fadeInUpBig animated animated" data-count="<?php echo totalAdmins(); ?>" style="visibility: visible;">
                    <i class="fa fa-users"></i>
                    <p id="number1" class="number"><?php echo totalAdmins(); ?></p>
                    <span></span>
                    <p>Users</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-count="<?php echo totalPosts(); ?>" style="visibility: visible;">
                    <i class="fa fa-sticky-note"></i>
                    <p id="number2" class="number"><?php echo totalPosts(); ?></p>
                    <span></span>
                    <p>Posts</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" style="visibility: visible;">
                    <i class="fa fa-list-alt"></i>
                    <p id="number3" class="number" data-count="<?php echo totalCategories(); ?>"><?php echo totalCategories(); ?></p>
                    <span></span>
                    <p>Category</p>
                </div>
                <div class="item wow fadeInUpBig animated animated" data-count="<?php echo totalComments(); ?>" style="visibility: visible;">
                    <i class="fa fa-comment"></i>
                    <p id="number4" class="number"><?php echo totalComments(); ?></p>
                    <span></span>
                    <p>Comments</p>
                </div>
            </div>
        </div>`
    </div>
</div>
<!--About Us section-->
<section id="about" class="about-section">
    <div class="container">
        <div class="row">
            <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                <div class="inner-column">
                    <div class="sec-title">
                        <span class="title">About CodeWarriors</span>
                        <h2>We are leader in <br>blogger services</h2>
                    </div>
                    <div class="text">
                        Welcome to CodeWarriors, your number one source for all things. We're dedicated to providing you the very best of blog services, with an emphasis on posts, categories and problems.
                        Founded in 2019 by Lorenc & Mexhit, CodeWarriors has come a long way from its beginnings in Albania. When CEOs first started out, their passion for friendly blog with all services drove them to start their own ideas.
                        In partnership with Marinel and Naum they make a team with very good skills. New ideas with a good experience makes them a fantastic team.
                        We hope you enjoy our blog services as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us.
                    </div>
                    <ul class="list-style-one">
                        <li>Get great posts right now</li>
                        <li>Create your own profile and make your own posts</li>
                        <li>Get likes or comments and make yourself a top blogger</li>
                    </ul>
                    <div class="btn-box">
                        <a href="#contact" class="theme-btn btn-style-one">Contact Us</a>
                    </div>
                </div>
            </div>
            <!-- Image Column -->
            <div class="image-column col-lg-6 col-md-12 col-sm-12">
                <div class="inner-column wow fadeInLeft">
                    <figure class="image-1"><a href="#" class="lightbox-image" data-fancybox="images"><img src="images/aboutus1.jpg" class="about_us_1" alt=""></a></figure>
                    <figure class="image-2"><a href="#" class="lightbox-image" data-fancybox="images"><img src="images/cw_new.png" class="about_us_2" alt=""></a></figure>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact us section -->
<section id="contact" class="newsletter-section">
    <div class="container">
        <h2>Contact us</h2>
        <form id="contact_form" class="newsletter-form" action="https://script.google.com/macros/s/AKfycbz80rbLKV7pFEw_qgPwyZu4hKNQdyRGgZE5_MYvQjudhDjQl9_V/exec" method="post" data-email="from_email@example.com">
            <input type="text" id="inputName" name="name" placeholder="ENTER YOUR NAME">
            <input type="text" id="inputMessage" name="email" placeholder="ENTER YOUR E-MAIL">
            <br><br>
            <textarea rows="10" name="message" placeholder="ENTER YOUR MESSAGE"></textarea>
            <br><br>
            <button class="site-btn" id="contact_us" name="contact_us">contact us  <img src="images/icons/double-arrow.png" alt="#"/></button>
        </form>
    </div>
</section>
<!-- Footer section -->
<footer class="footer-section">
    <div class="container">
        <div class="footer-cta pt-5 pb-5">
            <div class="row">
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="cta-text">
                            <h4>Find</h4>
                            <span>1010 Tirane, Albania</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="fas fa-phone"></i>
                        <div class="cta-text">
                            <h4>Call</h4>
                            <span>+355 68 ** ** ***</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 mb-30">
                    <div class="single-cta">
                        <i class="far fa-envelope-open"></i>
                        <div class="cta-text">
                            <h4>Mail</h4>
                            <span>infocodewarriors@gmail.com</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-content pt-5 pb-5">
            <div class="row">
                <div class="col-xl-4 col-lg-4 mb-50">
                    <div class="footer-widget">
                        <div class="footer-logo">
                            <a href="login.php"><img src="images/cw.png" class="img-fluid" alt="logo"></a>
                        </div>
                        <div class="footer-social-icon">
                            <span>Follow us</span>
                            <a href="#"><i class="fab fa-facebook-f facebook-bg"></i></a>
                            <a href="#"><i class="fab fa-twitter twitter-bg"></i></a>
                            <a href="#"><i class="fab fa-google-plus-g google-bg"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Useful Links</h3>
                        </div>
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#posts">Posts</a></li>
                            <li><a href="#statistics">Statistics</a></li>
                            <li><a href="#about">About us</a></li>
                            <li><a href="#contact">Contact us</a></li>
                            <li><a href="login.php">Login/Register</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mb-50">
                    <div class="footer-widget">
                        <div class="footer-widget-heading">
                            <h3>Subscribe</h3>
                        </div>
                        <div class="footer-text mb-25">
                            <p>Don’t miss to subscribe to our new feeds, kindly fill the form below.</p>
                        </div>
                        <div class="subscribe-form">
                            <form action="#">
                                <input type="text" placeholder="Email Address">
                                <button><i class="fab fa-telegram-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='back-to-top' id='back-to-top' title='Back to top'><i class='fa fa-chevron-up'></i></div>
    <div style="height: 2px; background: #005cbf"></div>
    <div class="footer-bottom py-2">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="lead text-center m-0">© Copyright 2020 <b><a class="color" href="login.php">CodeWarriors</a></b>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
<!-- Javascripts & Jquery -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
<script data-cfasync="false" type="text/javascript" src="js/form-submission-handler.js"></script>
</html>