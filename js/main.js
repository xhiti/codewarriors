'use strict';

$(window).on('load', function() {
	/*------------------
		Preloder
	--------------------*/
	$(".loader").fadeOut();
	$("#preloder").delay(400).fadeOut("slow");

});

(function($) {
	/*------------------
		Navigation
	--------------------*/
	$('.primary-menu').slicknav({
		appendTo:'.header-warp',
		closedSymbol: '<i class="fa fa-angle-down"></i>',
		openedSymbol: '<i class="fa fa-angle-up"></i>'
	});

	/*------------------
		Background Set
	--------------------*/
	$('.set-bg').each(function() {
		var bg = $(this).data('setbg');
		$(this).css('background-image', 'url(' + bg + ')');
	});

	/*------------------
		Hero Slider
	--------------------*/
	$('.hero-slider').owlCarousel({
		loop: true,
		nav: true,
		dots: true,
		navText: ['', '<img src="images/icons/solid-right-arrow.png">'],
		mouseDrag: false,
		animateOut: 'fadeOut',
		animateIn: 'fadeIn',
		items: 1,
		//autoplay: true,
		autoplayTimeout: 10000,
	});

	var dot = $('.hero-slider .owl-dot');
	dot.each(function() {
		var index = $(this).index() + 1;
		if(index < 10){
			$(this).html('0').append(index + '.');
		}else{
			$(this).html(index + '.');
		}
	});
})(jQuery);

(function () {
	$(window).on('load', function () {
		$('.number').each(function () {
			$(this).prop('Counter',0).animate({
				Counter: $(this).text()
			}, {
				duration: 4000,
				easing: 'swing',
				step: function (now) {
					$(this).text(Math.ceil(now));
				}
			});
		});
	})
}).call(this);

$(function() {
	if ($('#sticky-sidebar').length) {
		var el = $('#sticky-sidebar');
		var stickyTop = $('#sticky-sidebar').offset().top;
		var stickyHeight = $('#sticky-sidebar').height();
		$(window).scroll(function() {
			var limit = $('#footer-wrapper').offset().top - stickyHeight - 20;
			var windowTop = $(window).scrollTop();
			if (stickyTop < windowTop) {
				el.css({
					position: 'fixed',
					top: 20
				});
			} else {
				el.css('position', 'static');
			}
			if (limit < windowTop) {
				var diff = limit - windowTop;
				el.css({
					top: diff
				});
			}
		});
	}
});

// Back to top button
(function () {
	$(document).ready(function () {
		return $(window).scroll(function () {
			return $(window).scrollTop() > 600 ? $("#back-to-top").addClass("show") : $("#back-to-top").removeClass("show")
		}), $("#back-to-top").click(function () {
			return $("html,body").animate({
				scrollTop: "0"
			})
		})
	})
}).call(this);

/*Replies*/
$(document).on('click', '.btn-reply', function(eve){
	eve.preventDefault();
	$(this).parent().parent().siblings('.comment-footer').slideToggle();
	eve.stopImmediatePropagation();
	console.log($(this));
});

$(document).on('click', '.btn-send', function(eve){
	var targetObject = $(this).parent().parent().parent().parent().parent();
	//console.log(targetObject);
	var reply_text = $(this).parent().siblings('textarea').val();

	if($.trim(reply_text) == " " || $.trim(reply_text) == ""){
		alert("insert comment");
	} else {
		if($(targetObject).hasClass("comment-main-level"))
		{
			if($(targetObject).siblings('.comments-list.reply-list')) {
				let element_prepend = '<li> <div class="comment-avatar"><img alt="" src="http://dummyimage.com/60"></div><div class="comment-box"> <div class="comment-head"> <h6 class="comment-name"><a href="#">Lorena Rojero</a></h6> <span class="posted-time">Posted on DD-MM-YYYY HH:MM</span> <i class="fa fa-reply"></i> <i class="fa fa-heart"></i> </div> <div class="comment-content">'+ reply_text +' <div class="comment-open"> <a class="btn-reply"> <i class="fa fa-reply"></i> </a> </div> </div> <div class="comment-footer"> <div class="comment-form"> <textarea id="" name="" class="form-control"></textarea> <div class="pull-right send-button"> <a class="btn-send">send</a> </div> </div> </div> </div> </li>';
				$(targetObject).siblings('.comments-list.reply-list').prepend(element_prepend);
			}
		}
	}
});

/*change pass js*/
$('.unmask').on('click', function(){
	if($(this).prev('input').attr('type') == 'password')
		$(this).prev('input').prop('type', 'text');
	else
		$(this).prev('input').prop('type', 'password');
	return false;
});
//Begin supreme heuristics
$('.password').on('keyup',function (){
	var p_c = $('#p-c');
	var p = $('#p');
	console.log(p.val() + p_c.val());
	if(p.val().length > 0){
		if(p.val() != p_c.val()) {
			$('#valid').html("Passwords Don't Match");
		} else {
			$('#valid').html('');
		}
		var s = 'weak'
		if(p.val().length > 5 && p.val().match(/\d+/g))
			s = 'medium';
		if(p.val().length > 6 && p.val().match(/[^\w\s]/gi))
			s = 'strong';
		$('#strong span').addClass(s).html(s);
	}
});

/*DASHBOARD*/
$(function() {
	$('.chart').easyPieChart({
		/*scaleColor: "#0090DB",*/
		lineWidth: 20,
		lineCap: 'butt',
		barColor: '#0C0613',
		trackColor:	"#0090DB",
		size: 180,
		animate: 500
	});
});

function selectMetric(e) {
	e.preventDefault();
	var metric = $(e.currentTarget).attr('data-metric');
	var metricSelector = '[data-metric="' + metric + '"]';

	// set selected link
	$('.side .selected').removeClass('selected');
	$('.side .links a' + metricSelector).addClass('selected');

	// show proper stats
	$('.side .stats ul').hide();
	$('.side .stats ul' + metricSelector).show();

	// activate proper graph
	var $wrapper = $('.main article' + metricSelector).parent();
	var isActive = $wrapper.hasClass('active');
	if (!isActive) {
		$wrapper
			.addClass('active')
			.siblings().removeClass('active');
	}
}

function checkKey(e) {
	if (e.keyCode === 13) {
		// hit enter
		selectMetric(e);
	}
}

$('.side .links a').on('click', selectMetric);
$('.main article').on({
	'click': selectMetric,
	'keyup': checkKey
});



$('.main-nav ul li').click(function() {
	$('.main-nav ul li').removeClass('active');
	$(this).addClass('active');
});

$('.menuicon').click(function() {
	$('.ul-menu').css('bottom', '-150%');
	$(this).next('.ul-menu').css('bottom', '0');
});

$('.ul-menu-close').click(function() {
	$('.ul-menu').css('bottom', '-150%');
});


function showTime(){
	var date = new Date();
	var h = date.getHours(); // 0 - 23
	var m = date.getMinutes(); // 0 - 59
	var s = date.getSeconds(); // 0 - 59
	var session = "AM";

	if(h == 0){
		h = 12;
	}

	if(h > 12){
		h = h - 12;
		session = "PM";
	}

	h = (h < 10) ? "0" + h : h;
	m = (m < 10) ? "0" + m : m;
	s = (s < 10) ? "0" + s : s;

	var time = h + ":" + m + ":" + s + " " + session;
	document.getElementById("MyClockDisplay").innerText = time;
	document.getElementById("MyClockDisplay").textContent = time;

	setTimeout(showTime, 1000);

}

showTime();
