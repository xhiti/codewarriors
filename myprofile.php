<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>
<?php confirmLogin(); ?>

<?php
    $username = $_SESSION['username'];
    $sql = "select * from admins where username = '$username'";
    $result = mysqli_query($connectingDB, $sql);
    $row = mysqli_fetch_array($result);
    $user_id = $row[0];
    $role = $row['role'];

    if ($result){
        $username  = $row['username'];
        $name      = $row['aname'];
        $age       = $row['age'];
        $email     = $row['email'];
        $location  = $row['location'];
        $bio       = $row['abio'];
        $interests = $row['aheadline'];
    }

    if (isset($_POST['edit_button'])){
        $username_edit = $_POST['username'];
        $name_edit = $_POST['user_name'];
        $email_edit = $_POST['email'];
        $age_edit = $_POST['age'];
        $location_edit = $_POST['location'];
        $bio_edit = $_POST['bio'];
        $interests_edit = $_POST['interests'];
        $image_edit = $_FILES["image_edit"]["name"];
        $target = "upload/".basename($_FILES["image_edit"]["name"]);

        date_default_timezone_set("Europe/London");
        $currentTime = time();
        $dateTime = strftime("%B-%d-%Y %H:%M:%S", $currentTime);

        if (empty($name_edit)){
            $_SESSION["ErrorMessage"] = "Name must be filled out!";
        }
        elseif (strlen($name_edit) < 3){
            $_SESSION["ErrorMessage"] = "Name should be greater than 3 characters!";
        }
        else{
            if (empty($username_edit)){
                $name_edit = $name;
            }
            if (empty($name_edit)){
                $name_edit = $name;
            }
            if (empty($bio_edit)){
                $bio_edit = $bio;
            }
            if (empty($interests_edit)){
                $interests_edit = $interests;
            }
            if (empty($email_edit)){
                $email_edit = $email;
            }
            if (!empty($_FILES["image_edit"]["name"])) {
                $sql = "UPDATE admins 
                       SET dateTime   = '$dateTime',
                           username   = '$username_edit',
                           aname      = '$name_edit',
                           email      = '$email_edit', 
                           aimage     = '$image_edit',
                           aheadline  = '$interests_edit',
                           abio       = '$bio_edit',
                           age        = '$age_edit',
                           location   = '$location_edit'
                       WHERE id       = '$user_id'";
                move_uploaded_file($_FILES["image_edit"]["tmp_name"], $target);
            }
            else{
                $sql = "UPDATE admins 
                       SET dateTime   = '$dateTime',
                           username   = '$username_edit',
                           aname      = '$name_edit', 
                           email      = '$email_edit', 
                           aheadline  = '$interests_edit',
                           abio       = '$bio_edit',
                           age        = '$age_edit',
                           location   = '$location_edit'
                       WHERE id       = '$user_id'";
            }
            $result = mysqli_query($connectingDB, $sql);

            if ($result){
                $_SESSION['username'] = $username_edit;
                $_SESSION["SuccessMessage"] = "Profile edited successfully!";
                header("Location: myprofile.php");
            }
            else{
                $_SESSION["ErrorMessage"] = "Something went wrong! Try again!";
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="images/first.jpg" rel="shortcut icon"/>
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/fontawesome.min.css">
    <link rel="stylesheet" href="fontawesome-free-5.12.1-web/css/all.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/index.css">
    <title>CodeWarriors | My Profile</title>
    <style media="screen">
        .heading{
            font-family: Bitter,Georgia,"Times New Roman",Times,serif;
            font-weight: bold;
            color: #005E90;
        }
        .heading:hover{
            color: #0090DB;
        }

        .statistika{
            color:black;
            font-size:20px;
        }
    </style>
</head>
<body>
<!-- NAVBAR -->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="blog.php?page=1"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <?php
                global $role;
                $username = $_SESSION['username'];
                $sql      = "select * from admins where id = '$user_id'";
                $result   = mysqli_query($connectingDB, $sql);
                $row      = mysqli_fetch_array($result);
                $role     = $row['role'];

                if ($role === 'user'){
            ?>
            <ul class="sm-nav-menu">
                <li><a href="blog.php?page=1" class="nav-links">Blog</a></li>
                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                <li><a href="addNewPost.php" class="nav-links">Create post</a></li>
                <li><a href="aboutus.php" class="nav-links2">About Us</a></li>
                <li><a href="contactus.php" class="nav-links2">Contact Us</a></li>
                <li><a href="login.php" class="nav-links3">Logout</a></li>
            </ul>
            <ul style="float:right;" class="navbar-nav ml-auto">
                <form class="form-inline d-none d-sm-block" action="blog.php">
                    <div class="form-group">
                        <input class="form-control mr-2" type="text" name="Search" placeholder="Search here"value="">
                        <button  class="btn btn-primary" name="SearchButton">Go</button>
                    </div>
                </form>
            </ul>
            <?php }
                elseif ($role === 'admin') { ?>
                <nav class="sm-navbar navbar navbar-expand-lg">
                    <div class="container2">
                        <div class="sm-logo">
                            <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
                            <ul class="sm-nav-menu" style="float: right; width: 100%; margin: 0;">
                                <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                                <li><a href="posts.php" class="nav-links2">Posts</a></li>
                                <li><a href="categories.php" class="nav-links2">Categories</a></li>
                                <li><a href="admins.php" class="nav-links2">Manage Users</a></li>
                                <li><a href="comments.php" class="nav-links2">Comments</a></li>
                                <li><a href="blog.php?page=1" class="nav-links2">Live Blog</a></li>
                                <li><a href="login.php" class="nav-links3">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            <?php } ?>
        </div>
    </div>
</nav>
<?php
    global $role;
    $username = $_SESSION['username'];
    $sql      = "select * from admins where id = '$user_id'";
    $result   = mysqli_query($connectingDB, $sql);
    $row      = mysqli_fetch_array($result);
    $role     = $row['role'];

    if ($role === 'user'){
        ?>
<div style="height:70px; background:#27aae1;"></div>
<!-- NAVBAR END -->
<!-- HEADER -->

<div class="container">
    <div class="row mt-4">
        <!-- Side Area Start -->
        <div class="col-sm-4">
            <h2 style="color: #0C0613; padding-top: 20px;">My <span style="font-weight: bold">Profile</span></h2>
            <br><br>
            <div class="card">
                <div class="profile grid-area">
                    <?php
                        $sql = "SELECT * FROM admins a WHERE a.id = '$user_id'";
                        $result = mysqli_query($connectingDB, $sql);
                        $row = mysqli_fetch_array($result);
                        $image = $row['aimage'];
                    ?>
                    <div class="img">
                        <img src="upload/<?php echo $image; ?>">
                        <h3 style="color: #0C0613; font-weight: bold"><?php echo $username; ?></h3>
                        <div class="button"><a href="#edit"><i class="fa fa-user"></i></a></div>
                    </div>
                    <div class="profile-data">
                        <div class="data-details">
                            <h5 style="color: #0C0613; font-weight: bold">Age</h5>
                            <h4>
                            <?php
                                echo '<h4>'.$age.'</h4>';
                            ?>
                            </h4>
                        </div>
                        <div class="data-details">
                            <h5 style="color: #0C0613; font-weight: bold">Location</h5>
                            <h4>
                            <?php
                                echo '<h4>'.$location.'</h4>';
                            ?>
                            </h4>
                        </div>
                        <div class="data-details">
                            <h5 style="color: #0C0613; font-weight: bold">Posts</h5>
                            <?php
                                $sql = "SELECT * FROM posts p join admins a on a.id = p.user_id where a.id = $user_id";
                                $result = mysqli_query($connectingDB, $sql);
                                $row = mysqli_num_rows($result);
                                echo '<h4>'.$row.'</h4>';
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="card">
                <div class="pl20 pb30 clear" style="padding: 30px;">
                    <h3 style="font-weight: bold">Your Posts (latest 6)</h3>
                    <div class="trending-widget" style="padding-top: 30px;">
                        <div class="trending-widget" style="padding-top: 30px;">
                            <?php
                            global $connectingDB;
                            $sql= "SELECT * FROM posts p join admins a on a.id = p.user_id where a.id = $user_id ORDER BY p.id desc LIMIT 0,6;";
                            $result = mysqli_query($connectingDB, $sql);
                            while ($DataRows = mysqli_fetch_array($result)) {
                                $Id     = $DataRows[0];
                                $Title  = $DataRows['title'];
                                $DateTime = $DataRows['dateTime'];
                                $Image = $DataRows['image'];
                                ?>
                                <div class="tw-item" style="border-left: 5px solid #27aae1; margin-bottom: 10px">
                                    <div class="tw-thumb">
                                        <img src="upload/<?php echo htmlentities($Image); ?>" class="trending_posts" style="width: 115px; height: 78px;" alt="#">
                                    </div>
                                    <div class="tw-text">
                                        <div class="tw-meta" style="font-size: 8px"><?php echo htmlentities($DateTime); ?></div>
                                        <a style="text-decoration:none;" href="fullPost.php?id=<?php echo htmlentities($Id) ; ?>" target="_blank"><h5 style="font-weight: bold; color: #0C0613"><?php if (strlen($Title) > 20) { $Title = substr($Title, 0, 20)."..."; } echo htmlentities($Title); ?></h5></a>
                                    </div>
                                </div>
                                <br>
                            <?php } ?>
                        </div>
                        <div class="tw-item" style="margin-bottom: 10px">
                            <a href="posts_user.php" style="font-weight: bold; padding-left: 37%; color: #0C0613">All posts &rang;&rang;</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Side Area End -->
        <!-- Main Area Start-->
        <div class="col-sm-8 "> <br>
            <h2 style="color: #0C0613; float: right; padding-bottom: 40px;">CodeWarriors <span style="font-weight: bold;">Blog</span></h2>
            <div class="card" style="width: 100%; margin-top: 55px">
                <!-- <h1 style="color: #0C0613; font-weight: bold">CODE WARRIORS Blog</h1> <br><br> -->
                <div style="background-color: white">
                    <div class="edit-profile grid-area">
                        <?php
                        $sql = "SELECT * FROM admins a WHERE a.id = '$user_id'";
                        $result = mysqli_query($connectingDB, $sql);
                        if ($result){
                            while ($row = mysqli_fetch_array($result)) {
                                unset($title);
                                $post_id = $row[0];
                                $username = $row['username'];
                                $name = $row['aname'];
                                $interests = $row['aheadline'];
                                $bio = $row['abio'];
                                $image = $row['aimage'];
                                ?>
                                <div class="profile-picture">
                                    <img src="upload/<?php echo $image; ?>">
                                </div>
                                <div style="border-left: 10px solid #0C0613;">
                                    <h1 style="font-weight: bold; padding-top: 0px;"><?php echo htmlentities($name); ?> <i class="fas fa-pen" style="cursor: pointer"></i></h1><br>
                                </div>
                                <div class="edit-infos">
                                    <h3 style="font-weight: bold">Personal information</i></h3><br>
                                    <div class="profile-data">
                                        <div class="data-details">
                                            <h5 style="font-weight: bold">Age</h5>
                                            <h4>
                                                <?php
                                                    echo '<h4>'.$age.'</h4>';
                                                ?>
                                            </h4>
                                        </div>
                                        <div class="data-details">
                                            <h5 style="font-weight: bold">Username</h5>
                                            <h4>
                                                <?php
                                                echo '<h4>'.$username.'</h4>';
                                                ?>
                                            </h4>
                                        </div>
                                        <div class="data-details">
                                            <h5 style="font-weight: bold">Location</h5>
                                            <h4>
                                                <?php
                                                    echo '<h4>'.$location.'</h4>';
                                                ?>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="edit-interest">
                                    <h3 style="font-weight: bold">Bio</i></h3>
                                    <p style="font-size: 20px">
                                        <?php
                                            if (empty($bio)){
                                                echo "-";
                                            }
                                            else{
                                                echo htmlentities($bio);
                                            }
                                        ?>
                                    </p>
                                </div>
                                <div class="edit-interest">
                                    <h3 style="font-weight: bold">Your interests</i></h3>
                                    <p style="font-size: 20px">
                                        <?php
                                            if (empty($interests)){
                                                echo "-";
                                            }
                                            else{
                                                echo htmlentities($interests);
                                            }
                                        ?>
                                    </p>
                                </div><br><?php } } ?>
                    </div>
                </div>
            </div><br><br>
            <div class="card" id="edit">
                <div class="col-md-12" style="padding: 20px; background-color: #f6faff">
                    <form role="form" action="myprofile.php" method="post" enctype="multipart/form-data">
                        <h3 style="font-weight: bold">Edit your profile</h3><p>It's always easy</p>
                        <hr class="colorgraph">
                        <?php echo errorMessage(); echo successMessage(); ?>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <b>First Name:</b>
                                    <input type="text" id="first_name" name="user_name" class="form-control input-lg" placeholder="First Name" tabindex="1" value="<?php echo $name; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <b>Username:</b>
                                    <input type="text" id="username" name ="username" class="form-control input-lg" placeholder="Username" tabindex="2" value="<?php echo $username; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="form-group">
                                    <b>Email:</b>
                                    <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="3" value="<?php echo $email; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <b>Age:</b>
                                    <input type="text" name="age" id="age" class="form-control input-lg" placeholder="Age" tabindex="4" value="<?php echo $age; ?>">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <b>Location:</b>
                                    <input type="text" name="location" id="location" class="form-control input-lg" placeholder="Location" tabindex="5" value="<?php echo $location; ?>">
                                </div>
                            </div>
                        </div>
                        <b>Image:</b>
                        <div class="row" style="padding-left: 15px; padding-right: 15px;">
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="form-group">
                                    <input type="file" class="custom-file-input" name="image_edit" id="imageSelect" value="<?php echo $image; ?>">
                                    <label for="imageSelect" class="custom-file-label">Select Image</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <b>Bio:</b>
                                    <textarea name="bio" id="bio" class="form-control input-lg" placeholder="Bio..." tabindex="10"></textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <b>Interest:</b>
                                    <textarea name="interests" id="interests" class="form-control input-lg" placeholder="Interests..." tabindex="11"></textarea>
                                </div>
                            </div>
                        </div>
                        <hr class="colorgraph">
                        <div class="row">
                            <div class="col-xs-12 col-md-6"></div>
                            <div class="col-xs-12 col-md-6">
                                <button type="submit" name="edit_button" style="background-color: #0C0613" class="btn btn-success btn-block">
                                    <i class="fas fa-check"></i> Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><br><br>
            <div class="card" style="border: white">
                <div class="pl20 pb30 clear" style="padding: 30px;">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <a href="addNewPost.php" class="site-btn" id="contact_us" name="go_to_create_post" style="text-align: center; margin-left: 17.5%">Create Post  <img src="images/icons/double-arrow.png" alt="#"/></a>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <a href="change_password.php" class="site-btn" id="contact_us" name="change_pass" style="text-align: center; margin-left: 17.5%">Change Pass  <img src="images/icons/double-arrow.png" alt="#"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div id="posts" class="container3"><br>
    <div class="sectiontitle">
        <h2 style="font-weight: bold">Latest Posts</h2>
        <span class="headerLine"></span><br><br>
    </div><br><br>
    <section id="jarak">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators poindic">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="card-deck">
                            <?php
                            $sql = "select * from posts order by id desc limit 0,3";
                            $result = mysqli_query($connectingDB, $sql);

                            while ($row = mysqli_fetch_array($result)){
                                $post_id = $row['id'];
                                $title = $row['title'];
                                $dateTime = $row['dateTime'];
                                $image = $row['image'];
                                $post = $row['post'];
                                ?>
                                <div class="col">
                                    <div class="card">
                                        <img src="upload/<?php echo $image; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?php if (strlen($title) > 40) { $title = substr($title, 0, 40)."..."; } echo htmlentities($title); ?></h3>
                                            <p><?php if (strlen($post) > 115) { $post = substr($post, 0, 115)."..."; } echo htmlentities($post); ?></p>
                                        </div>
                                        <div class="card-footer">
                                            <small class="text-muted"><?php echo $dateTime; ?></a></small>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="card-deck">
                            <?php
                            $sql = "select * from posts order by id desc limit 0, 3";
                            $result = mysqli_query($connectingDB, $sql);

                            while ($row = mysqli_fetch_array($result)){
                                $post_id = $row['id'];
                                $title = $row['title'];
                                $dateTime = $row['dateTime'];
                                $image = $row['image'];
                                $post = $row['post'];
                                ?>
                                <div class="col">
                                    <div class="card">
                                        <img src="upload/<?php echo $image; ?>" class="card-img-top" alt="...">
                                        <div class="card-body">
                                            <h3><?php if (strlen($title) > 30) { $title = substr($title, 0, 30)."..."; } echo htmlentities($title); ?></h3>
                                            <p><?php if (strlen($post) > 100) { $post = substr($post, 0, 100)."..."; } echo htmlentities($post); ?></p>
                                        </div>
                                        <div class="card-footer">
                                            <small class="text-muted"><?php echo $dateTime; ?></a></small>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><br><br>
</div>
<div style="height: 30px;"></div>
<a href="blog.php?page=1" class="site-btn" id="contact_us" name="go_to_blog" style="text-align: center; margin-left: 44.5%;">Go to Blog  <img src="images/icons/double-arrow.png" alt="#"/></a>
<?php }
    else if ($role == "admin"){?>
        <div style="height:52px; background:#27aae1;"></div>
        <header class="bg-dark text-white py-3">
            <hr class="colorgraph" style="width: 100%!important;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 style="margin-top:-100px;" ><i class="fas fa-user" style="color: #27aae1;"></i> Admin Profile</h1>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="row mt-4">
                <!-- Side Area Start -->
                <div class="col-sm-4">
                    <h2 style="color: #0C0613; padding-top: 20px;">My <span style="font-weight: bold">Profile</span></h2>
                    <br><br>
                    <div class="card">
                        <div class="profile grid-area">
                            <?php
                            $sql = "SELECT * FROM admins a WHERE a.id = '$user_id'";
                            $result = mysqli_query($connectingDB, $sql);
                            $row = mysqli_fetch_array($result);
                            $image = $row['aimage'];
                            ?>
                            <div class="img">
                                <img src="upload/<?php echo $image; ?>">
                                <h3 style="color: #0C0613; font-weight: bold"><?php echo $username; ?></h3>
                                <div class="button"><a href="#edit"><i class="fa fa-user"></i></a></div>
                            </div>
                            <div class="profile-data">
                                <div class="data-details">
                                    <h5 style="color: #0C0613; font-weight: bold">Age</h5>
                                    <h4>
                                        <?php
                                        echo '<h4>'.$age.'</h4>';
                                        ?>
                                    </h4>
                                </div>
                                <div class="data-details">
                                    <h5 style="color: #0C0613; font-weight: bold">Location</h5>
                                    <h4>
                                        <?php
                                        echo '<h4>'.$location.'</h4>';
                                        ?>
                                    </h4>
                                </div>
                                <div class="data-details">
                                    <h5 style="color: #0C0613; font-weight: bold">Posts</h5>
                                    <?php
                                    $sql = "SELECT * FROM posts p join admins a on a.id = p.user_id where a.id = $user_id";
                                    $result = mysqli_query($connectingDB, $sql);
                                    $row = mysqli_num_rows($result);
                                    echo '<h4>'.$row.'</h4>';
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br><br>
                        <div class="card" style="width: 100%; margin-top: 55px">
                            <!-- <h1 style="color: #0C0613; font-weight: bold">CODE WARRIORS Blog</h1> <br><br> -->
                            <div style="background-color: white">
                                <div class="edit-profile grid-area">
                                    <?php
                                    $sql = "SELECT * FROM admins a WHERE a.id = '$user_id'";
                                    $result = mysqli_query($connectingDB, $sql);
                                    if ($result){
                                        while ($row = mysqli_fetch_array($result)) {
                                            unset($title);
                                            $post_id = $row[0];
                                            $username = $row['username'];
                                            $name = $row['aname'];
                                            $email = $row['email'];
                                            $interests = $row['aheadline'];
                                            $bio = $row['abio'];
                                            $image = $row['aimage'];
                                            ?>
                                            <div style="border-left: 10px solid #0C0613;">
                                                <h1 style="font-weight: bold; padding-top: 0px;"><?php echo htmlentities($name); ?> <i class="fas fa-pen" style="cursor: pointer"></i></h1><br>
                                            </div>
                                            <div class="edit-interest">
                                                <h3 style="font-weight: bold">Email:</i></h3>
                                                <p style="font-size: 20px">
                                                    <?php
                                                        echo htmlentities($email);
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="edit-interest">
                                                <h3 style="font-weight: bold">Bio:</i></h3>
                                                <p style="font-size: 20px">
                                                    <?php
                                                    if (empty($bio)){
                                                        echo "-";
                                                    }
                                                    else{
                                                        echo htmlentities($bio);
                                                    }
                                                    ?>
                                                </p>
                                            </div>
                                            <div class="edit-interest">
                                                <h3 style="font-weight: bold">Your interests:</i></h3>
                                                <p style="font-size: 20px">
                                                    <?php
                                                    if (empty($interests)){
                                                        echo "-";
                                                    }
                                                    else{
                                                        echo htmlentities($interests);
                                                    }
                                                    ?>
                                                </p>
                                            </div><br><?php } } ?>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- Side Area End -->
                <!-- Main Area Start-->
                <div class="col-sm-8 "> <br>
                    <h2 style="color: #0C0613; float: right; padding-bottom: 40px;">CodeWarriors <span style="font-weight: bold;">Admin</span></h2>
                    <br><br><br><br>
                    <div class="card" id="edit" style="width: 100%!important;">
                        <div class="col-md-12" style="padding: 20px; background-color: #f6faff">
                            <form role="form" action="myprofile.php" method="post" enctype="multipart/form-data">
                                <h3 style="font-weight: bold">Edit your profile</h3><p>It's always easy</p>
                                <hr class="colorgraph">
                                <?php echo errorMessage(); echo successMessage(); ?>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <b>First Name:</b>
                                            <input type="text" id="first_name" name="user_name" class="form-control input-lg" placeholder="First Name" tabindex="1" value="<?php echo $name; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <b>Username:</b>
                                            <input type="text" id="username" name ="username" class="form-control input-lg" placeholder="Username" tabindex="2" value="<?php echo $username; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-12">
                                        <div class="form-group">
                                            <b>Email:</b>
                                            <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="3" value="<?php echo $email; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <b>Age:</b>
                                            <input type="text" name="age" id="age" class="form-control input-lg" placeholder="Age" tabindex="4" value="<?php echo $age; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <b>Location:</b>
                                            <input type="text" name="location" id="location" class="form-control input-lg" placeholder="Location" tabindex="5" value="<?php echo $location; ?>">
                                        </div>
                                    </div>
                                </div>
                                <b>Image:</b>
                                <div class="row" style="padding-left: 15px; padding-right: 15px;">
                                    <div class="col-xs-12 col-sm-6 col-md-12">
                                        <div class="form-group">
                                            <input type="file" class="custom-file-input" name="image_edit" id="imageSelect" value="<?php echo $image; ?>">
                                            <label for="imageSelect" class="custom-file-label">Select Image</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <b>Bio:</b>
                                            <textarea name="bio" id="bio" class="form-control input-lg" placeholder="Bio..." tabindex="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <b>Interest:</b>
                                            <textarea name="interests" id="interests" class="form-control input-lg" placeholder="Interests..." tabindex="11"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr class="colorgraph">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6"></div>
                                    <div class="col-xs-12 col-md-6">
                                        <button type="submit" name="edit_button" style="background-color: #0C0613" class="btn btn-success btn-block">
                                            <i class="fas fa-check"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div><br><br>
                    <div class="card" style="border: white">
                        <div class="pl20 pb30 clear" style="padding: 30px;">
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <a href="dashboard.php" class="site-btn" id="contact_us" name="go_to_create_post" style="text-align: center; margin-left: 17.5%">Go to Dashboard  <img src="images/icons/double-arrow.png" alt="#"/></a>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <a href="change_password.php" class="site-btn" id="contact_us" name="change_pass" style="text-align: center; margin-left: 17.5%">Change Pass  <img src="images/icons/double-arrow.png" alt="#"/></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div id="posts" class="container3"><br>
            <div class="sectiontitle">
                <h2 style="font-weight: bold">Latest Posts</h2>
                <span class="headerLine"></span><br><br>
            </div><br><br>
            <section id="jarak">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators poindic">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="row">
                                <div class="card-deck">
                                    <?php
                                    $sql = "select * from posts order by id desc limit 0,3";
                                    $result = mysqli_query($connectingDB, $sql);

                                    while ($row = mysqli_fetch_array($result)){
                                        $post_id = $row['id'];
                                        $title = $row['title'];
                                        $dateTime = $row['dateTime'];
                                        $image = $row['image'];
                                        $post = $row['post'];
                                        ?>
                                        <div class="col">
                                            <div class="card">
                                                <img src="upload/<?php echo $image; ?>" class="card-img-top" alt="...">
                                                <div class="card-body">
                                                    <h3><?php if (strlen($title) > 40) { $title = substr($title, 0, 40)."..."; } echo htmlentities($title); ?></h3>
                                                    <p><?php if (strlen($post) > 115) { $post = substr($post, 0, 115)."..."; } echo htmlentities($post); ?></p>
                                                </div>
                                                <div class="card-footer">
                                                    <small class="text-muted"><?php echo $dateTime; ?></a></small>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="card-deck">
                                    <?php
                                    $sql = "select * from posts order by id desc limit 0, 3";
                                    $result = mysqli_query($connectingDB, $sql);

                                    while ($row = mysqli_fetch_array($result)){
                                        $post_id = $row['id'];
                                        $title = $row['title'];
                                        $dateTime = $row['dateTime'];
                                        $image = $row['image'];
                                        $post = $row['post'];
                                        ?>
                                        <div class="col">
                                            <div class="card">
                                                <img src="upload/<?php echo $image; ?>" class="card-img-top" alt="...">
                                                <div class="card-body">
                                                    <h3><?php if (strlen($title) > 30) { $title = substr($title, 0, 30)."..."; } echo htmlentities($title); ?></h3>
                                                    <p><?php if (strlen($post) > 100) { $post = substr($post, 0, 100)."..."; } echo htmlentities($post); ?></p>
                                                </div>
                                                <div class="card-footer">
                                                    <small class="text-muted"><?php echo $dateTime; ?></a></small>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><br><br>
        </div>
        <div style="height: 30px;"></div>
        <a href="blog.php?page=1" class="site-btn" id="contact_us" name="go_to_blog" style="text-align: center; margin-left: 44.5%;">Go to Blog  <img src="images/icons/double-arrow.png" alt="#"/></a>
    <?php } ?>
<br>
<br>
<br>
<?php require("includes/footer.php"); ?>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
</html>