<?php require_once("includes/db.php"); ?>
<?php require_once("includes/functions.php"); ?>
<?php require_once("includes/sessions.php"); ?>

<?php
    $_SESSION["TrackingURL"]=$_SERVER["PHP_SELF"];
    //confirmLogin();
?>

<?php ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>CodeWarriors | Users</title>
    <meta charset="UTF-8">
    <!-- Favicon -->
    <link href="images/first.jpg" rel="shortcut icon"/>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="css/about.css"/>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/footer.css">
    <!-- Main Stylesheets -->
    <link rel="stylesheet" href="css/index.css"/>
    <style>
        .list-wrapper {
            max-width: 400px;
            margin: 50px auto;
        }
        .list {
            background: #fff;
            border-radius: 2px;
            list-style: none;
            padding: 10px 20px;
        }
        .list-item {
            display: flex;
            margin: 10px;
            padding-bottom: 5px;
            padding-top: 5px;
            border-bottom: 1px solid rgba(0, 0, 0, 0.1);
        }
        .list-item:last-child {
            border-bottom: none;
        }
        .list-item-image {
            border-radius: 50%;
            width: 70px;
            height: 70px;
        }
        .list-item-content {
            margin-left: 20px;
        }
        .list-item-content h4, .list-item-content p {
            margin: 0;
        }
        .list-item-content h4 {
            margin-top: 10px;
            font-size: 18px;
        }
        .list-item-content p {
            margin-top: 5px;
            color: #aaa;
        }
    </style>
</head>
<body>
<!-- Header section -->
<nav class="sm-navbar navbar navbar-expand-lg">
    <div class="container2">
        <div class="sm-logo">
            <a href="index.php"><img src="images/cw.png" width="110px" height="40px"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarcollapseCMS">
            <ul class="sm-nav-menu" style="float: right; width: 100%; margin: 0;">
                <li><a href="dashboard.php" class="nav-links">Dashboard</a></li>
                <li><a href="myprofile.php" class="nav-links2">My Profile</a></li>
                <li><a href="posts.php" class="nav-links2">Posts</a></li>
                <li><a href="categories.php" class="nav-links2">Categories</a></li>
                <li><a href="admins.php" class="nav-links2">Manage Users</a></li>
                <li><a href="comments.php" class="nav-links2">Comments</a></li>
                <li><a href="blog.php?page=1" class="nav-links2">Live Blog</a></li>
                <li><a href="login.php" class="nav-links3">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>
<div style="height:50px; background:#27aae1;"></div>
<header class="bg-dark text-white py-3">
    <hr class="colorgraph" style="width: 100%!important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="margin-top:-100px;" ><i class="fas fa-user" style="color: #27aae1;"></i> Manage Users</h1>
            </div>
        </div>
    </div>
</header>
<br><br>
<section class="container3">
    <div class="offset-lg-2 col-lg-12" style="width: 100%; margin-left: 0!important;">
        <table class="table table-bordered " >
            <thead style="background-color: #0C0613!important; color: white;">
                <th>#</th>
                <th>Profile picture</th>
                <th>Username</th>
                <th>Name</th>
                <th>Email</th>
                <th>Age</th>
                <th>Location</th>
                <th># of posts</th>
                <th># of comments</th>
                <th>Action</th>
            </thead>
            <?php
            $sql_username="SELECT id,aname,email,username,aimage,location,age FROM admins WHERE role='user' ORDER BY aname";
            $result_username=mysqli_query($connectingDB,$sql_username);
            if (mysqli_num_rows($result_username)>0) {
              $i=1;
                while ($row_username=mysqli_fetch_assoc($result_username)){?>
                    <tr >
                        <td class="table-secondary"><b style="font-size:25px"><?php echo $i; ?></b></td>
                        <td class="table-secondary"><img src="<?php echo "upload/".$row_username['aimage']; ?>      " class="list-item-image" width="50px!important" height="50px!important"></td>
                        <td class="table-secondary" style="font-size:20px"><?php  echo $row_username['username'];?></td>

                        <?php
                        $sql_user_id="SELECT id FROM admins WHERE username='".$row_username['username']."'";
                        $result_user_id=mysqli_query($connectingDB,$sql_user_id);
                        $row_user_id=mysqli_fetch_assoc($result_user_id);

                        $sql_number_post="SELECT count(id) as c FROM posts WHERE user_id=".$row_user_id['id'];
                        $result_number_post=mysqli_query($connectingDB,$sql_number_post);
                        $row_number_post=mysqli_fetch_assoc($result_number_post);

                        $sql_number_comments="SELECT count(id) as c FROM comments WHERE comment_name='".$row_username['username']."'";
                        $result_number_comments=mysqli_query($connectingDB,$sql_number_comments);
                        $row_number_comments = mysqli_fetch_assoc($result_number_comments);
                        ?>
                        <td class="table-secondary"style="font-size:20px" ><?php echo $row_username['aname'] ?></td>
                        <td class="table-secondary" ><?php echo $row_username['email'] ?></td>
                        <td class="table-secondary"><b style="font-size:25px"><?php echo $row_username['age'] ?></b></td>
                        <td class="table-secondary" style="font-size:20px"><?php echo $row_username['location'] ?></td>
                        <td class="table-secondary" ><b style="font-size:25px"><?php echo $row_number_post['c'] ?></b></td>
                        <td class="table-secondary"><b style="font-size:25px"><?php echo $row_number_comments['c']; ?></b></td>
                        <td class="table-secondary"><a href="deleteAdmins.php?id=<?php echo $row_username['username']; ?> "><span class="btn btn-danger">Delete</span></a></td>
                    </tr>
                <?php $i=$i+1; }
            }?>
        </table>
    </div>
</section>
</div>
<br><br>
<!--END MAIN AREA-->
<?php require("includes/footer.php"); ?>
</body>
<!-- Javascripts & Jquery -->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.slicknav.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky-sidebar.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
</html>
